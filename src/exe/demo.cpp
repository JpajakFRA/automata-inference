/*
  demo.cpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation 

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>


#include <base/sample.h>
#include <base/solution.h>
#include <stdlib.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimberEval.h>
#include <eval/ILS.h>


using namespace std;


typedef pair<double, unsigned> Fitness2;

/* 
    Main
*/
int main(int argc, char ** argv) {
    // minimal example of sample
    //Sample sample("../instances/dfa_8_20_0.1_train-sample.json");

    /* print the sample if you need
    cout << "sample:" << endl;
    cout << sample << endl << endl;
    double nb_states = argv[3];
     Candidate solution with 4 states, 2 digits, and a fitness value which is correct classification rate

    //Solution<double> x(4, 2);

    cout << "Print an empty solution:" << endl;
    cout << x << endl << endl;

    Another candidate solution, read from file
    //Solution<double> xprime;

    // read from a file this solution
    //fstream filein("../instances/solution.json");

    //if (!filein)
      //  std::cerr << "Impossible to open solution.json" << std::endl;

    //filein >> xprime;

    //filein.close();

     print the solutions
    
    //cout << "Solution from file:" << endl;
    //cout << xprime << endl << endl;

    // Evaluation : transition function, and acceptance states are taken into account
    //              fitness is the classification rate
    //BasicEval eval(sample);

    //eval(xprime);

    cout << "Solutions after full evaluation:" << endl;
    cout << xprime << endl << endl;


     Evaluation : transition function is taken into account. Acceptance states are optimal ones.
                  fitness is the classification rate

    random generator with random seed 1


  	//SmartEval seval(gen, sample);

    //seval(xprime);

    cout << "solutions after function evaluation:" << endl;
    cout << xprime << endl << endl;

     Solution with fitness = (classifcaiton rate, nb of active states)
    //Solution< Fitness2 > x2;

    //fstream filein2("../instances/solution_biobj.json");

    //if (!filein2)
      //  std::cerr << "Impossible to open solution_biobj.json" << std::endl;

    //filein2 >> x2 ;

    //filein2.close();


	BasicBiobjEval eval2(sample);

    // evaluation of the solution
    x2.invalidate();
	eval2(x2);

    cout << x2 << endl;


     Evaluation : transition function is taken into account. Acceptance states are optimal ones.
                 fitness is the classification rate, and number of active states
  	SmartBiobjEval seval2(gen, sample);

    // evaluation of the solution
    seval2(x2);

    cout << "A candidate solution with bi-objective fitness value:" << endl;
    cout << x2 << endl<<endl;
    */

/////////////////////////////////////////////// MAIN
    Sample train(argv[1]);
    Sample test(argv[2]);
    int valeurarg = atoi(argv[3]);
    std::mt19937 gen(atoi(argv[4]));
  	Solution <double> sol(valeurarg, 2);

    std::uniform_int_distribution<int> rndsize(0, sol.nStates-1);
    for(int j = 0; j < sol.nStates; j++){
        for(int i = 0 ; i < 2 ; i++){
            sol.function[j][i] = rndsize(gen);
        }
    }

    SmartEval sevaltrain(gen, train);
    BasicEval evaltest(test);

    HillClimberEval hce(gen,50000);
                                                                     ///////////////////////////////////////////////HCL
    hce.evalFunc(sol,sevaltrain);
    cout << hce.best_sol.fitness() <<" ";


    evaltest(hce.best_sol);
    cout << hce.best_sol.fitness() << endl;


   /* ILS ils (gen,20);
    ils.evalFunc(sol,sevaltrain,hce);
    cout << ils.best_sol.fitness() <<" ";

    evaltest(ils.best_sol);
    cout << ils.best_sol.fitness() << endl;    */                      /////////////////////////////////////////////ILS

    //train avec algo puis 1 basiceval sur le test !


    return 0;
}