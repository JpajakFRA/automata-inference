//
// Created by jo on 07/12/2019.
//

#ifndef AUTOMATA_INFERENCE_ILS_H
#define AUTOMATA_INFERENCE_ILS_H
#include <base/DFA.h>
#include <base/solution.h>
#include <random>
#include "HillClimberEval.h"
#include "smartEval.h"

class ILS{
public:
    Solution<double> best_sol;
    int nbIterMax;
    std::mt19937 & gen;

    ILS(std::mt19937 & _gen,int _nbIterMax) : gen(_gen), nbIterMax(_nbIterMax){
    }

    void evalFunc(Solution<double> & sol,SmartEval seval,HillClimberEval hce){
        hce.evalFunc(sol,seval);
        best_sol = sol;
        int iter = 0;
        while(iter < nbIterMax){
            Solution <double> solprime (sol.nStates,2);
            std::uniform_int_distribution<int> rndsize(0, solprime.nStates-1);
            for(int j = 0; j < solprime.nStates; j++){
                for(int i = 0 ; i < 2 ; i++){
                    solprime.function[j][i] = rndsize(gen);
                }
            }
            hce.evalFunc(solprime,seval);
            if(best_sol.fitness() < solprime.fitness()){
                best_sol = solprime;
            }
            iter++;
        }
    }

};

#endif //AUTOMATA_INFERENCE_ILS_H
