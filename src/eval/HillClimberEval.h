//
// Created by jo on 03/11/2019.
//

#ifndef AUTOMATA_INFERENCE_HILLCLIMBEREVAL_H
#define AUTOMATA_INFERENCE_HILLCLIMBEREVAL_H

#include <base/DFA.h>
#include <base/solution.h>
#include <random>

class HillClimberEval {
public :

    std::mt19937 & gen;
    Solution<double> best_sol;
    int nbEvalMax;

    HillClimberEval(std::mt19937 & _gen,int _nbEvalMax) : gen(_gen),nbEvalMax(_nbEvalMax){
    }

    void evalFunc(Solution<double> & sol,SmartEval & seval) {
        std::uniform_int_distribution<int> rnd4(0, best_sol.nStates-1);
        std::uniform_int_distribution<int> rnd2(0, 1);

        int nbeval = 0;
        seval(sol);
        nbeval++;
        best_sol = sol;
        Solution<double> neighbor = sol;

        while(nbeval<nbEvalMax){
            int rnd_state = rnd4(gen);
            int rnd_symbol = rnd2(gen);

            neighbor.function[rnd_state][rnd_symbol] = (neighbor.function[rnd_state][rnd_symbol] + 1) % best_sol.nStates;

            seval(neighbor);
            nbeval++;
            if (neighbor.fitness() > best_sol.fitness())
                best_sol = neighbor;
            if(best_sol.fitness() == 1)
                break;
        }
    }

};

#endif //AUTOMATA_INFERENCE_HILLCLIMBEREVAL_H


