#!/bin/bash

>resultats_8_1.csv
for NUM in {0..29}
do
  echo $NUM
  for I in {0..50}
  do
  echo -n $I ' '
    ./build/demo instances/dfa_8_"$NUM"_0.1_train-sample.json instances/dfa_8_"$NUM"_0.1_test-sample.json 8 $I >> resultats_8_1.csv
  done
done
